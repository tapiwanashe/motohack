<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2beda1e8ab34ab6dec027f382d14b97c
{
    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'RedBeanPHP\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'RedBeanPHP\\' => 
        array (
            0 => __DIR__ . '/..' . '/gabordemooij/redbean/RedBeanPHP',
        ),
    );

    public static $prefixesPsr0 = array (
        'N' => 
        array (
            'NlpTools\\' => 
            array (
                0 => __DIR__ . '/..' . '/nlp-tools/nlp-tools/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2beda1e8ab34ab6dec027f382d14b97c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2beda1e8ab34ab6dec027f382d14b97c::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit2beda1e8ab34ab6dec027f382d14b97c::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
